package demo;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;

public class HelloController extends Controller {
    public void index() {
        renderText("Hello JFinal World.");
    }

    public void test() {
        renderText("Hello JFinal World. Test");
    }

    @ActionKey("/login")
    public void login() {
        render("login.html");
    }
} 